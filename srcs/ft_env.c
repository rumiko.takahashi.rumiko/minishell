/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:26:39 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 13:14:16 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** print all env
*/

void	ft_env(t_env *e)
{
	int		i;

	i = 0;
	while (e->env[i] != NULL)
		ft_putendl(e->env[i++]);
}

/*
** check corect env
*/

char	*ft_errchk(const char *n, const char *val, int owtire, t_env *e)
{
	char	*str;
	char	*strtmp;
	char	*fr;

	str = NULL;
	strtmp = NULL;
	fr = ft_getenv(n, e);
	if (n == NULL || n[0] == '\0' || ft_strchr(n, '=') != NULL ||
		val == NULL || ft_strchr(n, ' '))
		return (NULL);
	if (fr != NULL && owtire == 0)
		return (NULL);
	if (!(strtmp = ft_strjoin((char *)n, "=")))
		return (NULL);
	if (!(str = ft_strjoin(strtmp, (char *)val)))
		return (NULL);
	free(strtmp);
	if (str == NULL)
		return (NULL);
	if (fr != NULL)
		free(fr);
	return (str);
}

/*
** function for set env
*/

int		ft_setenv(const char *n, const char *val, int owtire, t_env *e)
{
	int		i;
	char	**tmp;
	char	*str;
	char	*strtmp;

	i = -1;
	strtmp = ft_errchk(n, val, owtire, e);
	while (e->env[++i] != NULL)
	{
		tmp = ft_strsplit(e->env[i], '=');
		if (ft_strncmp(tmp[0], n, ft_strlen(n)) == 0)
		{
			ft_strcpy(e->env[i], strtmp);
			ft_freearr(tmp);
			free(strtmp);
			return (0);
		}
		ft_freearr(tmp);
	}
	str = strtmp;
	e->env[i] = e->env[i - 1];
	e->env[i - 1] = str;
	e->env[i + 1] = NULL;
	return (0);
}

/*
** function for unsetenv from system
*/

int		ft_unsetenv(const char *name, t_env *e)
{
	int		i;
	char	**tmp;

	i = 0;
	while (e->env[i])
	{
		tmp = ft_strsplit(e->env[i], '=');
		if (ft_strncmp(tmp[0], name, ft_strlen(name)) == 0)
		{
			free(e->env[i]);
			while (e->env[i])
			{
				e->env[i] = e->env[i + 1];
				i++;
			}
			ft_freearr(tmp);
			return (0);
		}
		ft_freearr(tmp);
		i++;
	}
	return (0);
}
