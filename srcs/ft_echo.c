/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_echo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:41:29 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 17:16:40 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** printing env in echo
*/

static void		ft_printenv(char *str, t_env *e)
{
	int		i;
	int		j;
	char	tmp[255];
	char	*strtmp;

	i = 1;
	j = 0;
	ft_memset(tmp, 0, 255);
	while (str[i] != '\0')
	{
		tmp[j] = str[i];
		j++;
		i++;
	}
	strtmp = ft_getenv(tmp, e);
	if (strtmp == NULL)
		ft_putendl("Enviroment Variable does not exist");
	else
		ft_putendl(strtmp);
	if (strtmp != NULL)
		free(strtmp);
}

/*
** my simple realization of echo function
*/

int				ft_echo(t_env *e)
{
	char	*str;

	str = "\0";
	if (e->args[1] == NULL)
	{
		ft_putchar('\n');
		return (0);
	}
	str = ft_argsjoin(e->args);
	if (str[0] == '$')
		ft_printenv(str, e);
	else
	{
		ft_putendl(str);
		free(str);
	}
	return (0);
}
