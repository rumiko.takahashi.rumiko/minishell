/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:05:16 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 14:48:35 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** simple getch, read 1 symbol from 0 fd
*/

int		ft_getchar(void)
{
	char	c;

	if (read(0, &c, 1) != 1)
		return (-1);
	return ((int)c);
}

/*
** get line and return
*/

char	*ft_getline(void)
{
	int			i;
	int			c;
	static char	buff[BUFF_SIZE];

	i = 0;
	ft_memset(buff, 0, BUFF_SIZE);
	while (1)
	{
		c = ft_getchar();
		if (c == '\n' || c == '\0')
		{
			buff[i] = '\0';
			return (buff);
		}
		else
			buff[i] = c;
		i++;
	}
	free(buff);
	return (NULL);
}
