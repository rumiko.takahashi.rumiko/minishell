/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:08:57 by vbudnik           #+#    #+#             */
/*   Updated: 2018/06/30 15:22:17 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** handler signal from system
*/

void			proc_signal_handler(int signo)
{
	if (signo == SIGINT)
	{
		ft_putstr("\n");
		signal(SIGINT, proc_signal_handler);
	}
}

/*
** check and set corect arguments for execution proces
*/

static char		*ft_execute(t_env *v)
{
	int			i;
	int			p_count;
	char		tmp[2048];

	i = -1;
	p_count = 0;
	if (access(v->args[0], F_OK) != -1)
		return (ft_strdup(v->args[0]));
	if (v->path != NULL)
	{
		while (v->path[p_count])
			p_count++;
		while (i++ < p_count && v->path[i] != NULL)
		{
			ft_strcpy(tmp, v->path[i]);
			ft_strcat(tmp, "/");
			ft_strcat(tmp, v->args[0]);
			if (access(tmp, F_OK) != -1)
				return (ft_strdup(tmp));
			ft_bzero(tmp, ft_strlen(tmp));
		}
	}
	ft_putstr(v->args[0]);
	ft_putendl(": command not found");
	return (NULL);
}

/*
** fork proces and execution
*/

int				ft_fork(t_env *v)
{
	pid_t		pid;
	int			sig;
	char		*tmp;

	tmp = ft_execute(v);
	sig = 1;
	if (tmp != NULL)
	{
		pid = fork();
		if (pid < 0)
		{
			ft_putendl("error: forking failed");
			return (1);
		}
		if (pid > 0)
			wait(&sig);
		if (pid == 0)
			execve(tmp, v->args, v->env);
		free(tmp);
	}
	return (1);
}
